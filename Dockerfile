FROM debian
# server dependencies
RUN apt update && apt install -y git openssh-client nodejs yarnpkg
RUN ln -s /usr/bin/yarnpkg /usr/bin/yarn
# electron-forge building dependencies
RUN apt update && apt install -y zip wine mono-complete
RUN dpkg --add-architecture i386
RUN apt update && apt install -y wine32
WORKDIR /home
COPY ./server.js .
CMD ["/usr/bin/env", "node", "./server.js"]

# alpine doesn't support multilib (running 32bit from 64bit), see https://unix.stackexchange.com/a/420276/218172
# it is needed for electron-forge → winstaller → squirrel packager, results in "Bad EXE format"
# FROM alpine
# RUN apk add --no-cache git openssh nodejs yarn
# RUN apk add --no-cache zip wine mono libpng freetype
# # RUN ln /usr/bin/wine64 /usr/bin/wine
# RUN apk add --no-cache --repository http://dl-3.alpinelinux.org/alpine/edge/testing/ mono
