#!/usr/bin/node

const http = require('http')
const fs = require('fs')
const path = require('path')
const url = require('url')
const child_process = require('child_process')

let config = {
  port: 1234,
  sshkey: null,
  repository: 'https://github.com/adabru/electron-forge-ci-test.git'
}
// overwrite config with env params
for (let option of Object.keys(process.env).filter(option => option.startsWith('CI_'))) {
  let key = option.substring(3).toLowerCase()
  let value = process.env[option]
  config[key] = value
}
// overwrite config with cli params
for (let option of process.argv.filter(option => option.startsWith('--'))) {
  let split = option.indexOf('=')
  let key = option.substring(2, split)
  let value = option.substring(split + 1, option.length)
  config[key] = value
}
// write ssh key to file
if (config.sshkey != null) {
  fs.writeFileSync(__dirname + '/sshid', config.sshkey + '\n', { encoding: 'ascii', mode: 0o600 })
}

let host = '0.0.0.0'

let building = false
let buildOnceMore = false
let lastTag = null
let output = Buffer.from([])
let buildMessage = 'idle'

let buildDir = './clone/out/make'
let workingDir = __dirname

function message(value) {
  buildMessage = value
  console.log(value)
}

function abortBuild(_message) {
  message(_message)
  building = false
  if (buildOnceMore) {
    buildOnceMore = false
    rebuild()
  }
}

function cd(dir) { workingDir = dir }
async function runProcess(executable, ...args) {
  output = Buffer.from([])
  let stdout = Buffer.from([])
  let stderr = Buffer.from([])
  return await new Promise((res, rej) => {
    let child = child_process.spawn(executable, args, {
      cwd: workingDir,
      env: Object.assign({
        GIT_SSH_COMMAND: `ssh -o StrictHostKeyChecking=no -i ${__dirname + '/sshid'}`
      }, process.env)
    })
    child.stdout.on('data', data => { output += data ; stdout += data })
    child.stderr.on('data', data => { output += data ; stderr += data })
    child.on('error', e => rej(e))
    child.on('exit', (code, signal) => res({code, stdout, stderr}))
  })
}

async function rebuild(force = false) {
  if (building)
    return
  building = true

  try {
    cd(__dirname)

    let code, stdout, stderr

    // git clone
    if (!fs.existsSync('./clone')) {
      message(`cloning ${config.repository} into clone/ ...`)
      ;({code, stdout, stderr} = await runProcess('git', 'clone', config.repository, 'clone'))
      if (code != 0)
        return abortBuild(`cloning ${config.repository} failed!`)
    }

    cd(__dirname + '/clone')

    // reset changes
    message('reset tree')
    ;({code, stdout, stderr} = await runProcess('git', 'reset', '--hard', 'HEAD'))
    if (code != 0)
      return abortBuild(`resetting tree failed`)

    // git pull
    message('pulling changes')
    ;({code, stdout, stderr} = await runProcess('sh', '-c', 'git pull >/dev/null && git tag --contains HEAD'))
    if (code != 0)
      return abortBuild(`pulling failed`)

    let tag = stdout.toString().trim()
    if (tag == '')
      return abortBuild('no tag found, abort build')
    else if (tag == lastTag && !force)
      return abortBuild('this version was already successfully built')

    // yarn
    message('update app dependencies')
    ;({code, stdout, stderr} = await runProcess('yarn'))
    if (code != 0)
      return abortBuild(`yarn failed`)

    // set version from tag
    message('setting version')
    ;({code, stdout, stderr} = await runProcess('sed', '-i', `s/"version": "1.0.0"/ "version": "${tag}"/`, 'package.json'))
    if (code != 0)
      return abortBuild(`replacing version failed`)

    if (code != 0)
      return abortBuild(`yarn failed`)

    for (platform of ['win32', 'darwin', 'linux']) {
      message(`building version ${tag} for ${platform} since ${new Date().getHours()}:${new Date().getMinutes()}...`)
      ;({code, stdout, stderr} = await runProcess('./node_modules/.bin/electron-forge', 'make', '--platform', platform))
      if (code != 0)
        return abortBuild(`building for ${platform} failed!`)
    }
    lastTag = tag
  } catch(e) {
    output = 'ci-server ' + e.stack
    return abortBuild('ci-server failed with ' + e)
  }
  message('idle')
  output = Buffer.from([])
  building = false
  if (buildOnceMore) {
    buildOnceMore = false
    rebuild()
  }
}

async function renderFiletree(_path = buildDir) {
  try {
    let stats = await fs.promises.stat(_path)
    if (stats.isFile(_path))
      return `<li><a href="build/${_path.substring(buildDir.length+1)}">${path.basename(_path)}</a></li>`
    else {
      let files = await fs.promises.readdir(_path)
      let children = await Promise.all(files.map(async file => {
          return await renderFiletree(_path+'/'+file)
        }))
      return `<li>${path.basename(_path)}</li><ul>${children.join('\n')}</ul>`
    }
  } catch(e) {
    if (_path == buildDir)
      return `<h3>no build available</h3>`
    else
      return `<li>error accessing file</li>`
  }
}

async function getBuildFiles(_path = buildDir) {
  try {
    let stats = await fs.promises.stat(_path)
    if (stats.isFile(_path))
      return _path
    else {
      let files = await fs.promises.readdir(_path)
      let children = await Promise.all(files.map(async file => {
          return await getBuildFiles(_path+'/'+file)
        }))
      return [].concat(...children)
    }
  } catch(e) {
    return []
  }
}

async function getLatest() {
  let res = {}
  let builds = await getBuildFiles()
  for (platform of [{id:'win32',filter:'Setup.exe'}, {id:'darwin',filter:'zip/darwin'}, {id:'linux',filter:'zip/linux'}]) {
    res[platform.id] = {
      path: '',
      semver: [0,0,0]
    }
    for (build of builds.filter(x => x.includes(platform.filter))) {
      let versionstring = /[\d]+\.[\d]+\.[\d]+/.exec(build)
      if (versionstring) {
        let semver = versionstring[0].split('.').map(x => parseInt(x))
        if ( res[platform.id].semver[0] < semver[0]
          || res[platform.id].semver[0] == semver[0] && res[platform.id].semver[1] < semver[1]
          || res[platform.id].semver[0] == semver[0] && res[platform.id].semver[1] == semver[1] && res[platform.id].semver[2] < semver[2] )
          Object.assign(res[platform.id], {semver, path:build})
      }
    }
  }
  return res
}

let server = http.createServer( async (req, res) => {
  try {
    let _url = url.parse(req.url)

    // rewrite path for latest version
    if (_url.pathname.startsWith('/latest/')) {
      let platform = _url.pathname.substring('/latest/'.length)
      if (!['win32','darwin','linux'].includes(platform)) {
        res.writeHead(404)
        res.end(`unknown platform: ${platform}`)
      }
      let latest = await getLatest()
      _url.pathname = latest[platform].path.replace(buildDir, '/build')
    }
    // rewrite path for squirrel update
    else if (_url.pathname.startsWith('/win64/')) {
      _url.pathname = '/build/squirrel.windows/x64/' + _url.pathname.substring('/win64/'.length)
    }

    _url.pathname = unescape(_url.pathname)
    if(_url.pathname != path.normalize(_url.pathname)) {
      res.writeHead(400)
      res.end('invalid path')
    } else if(_url.pathname == '/') {
      res.writeHead(200)
      res.end(`
        <h1>electron build server</h1>
        <em>${buildMessage}</em>
        <pre>${output}</pre>
        <h5 id="version">version: </h5>
        <script>(async function(){
          let fetchversion = await fetch('version')
          let version = await fetchversion.text()
          document.querySelector('#version').innerText = 'version: ' + version
        })()</script>
        <a href="latest/win32">latest windows build</a>
        <a href="latest/darwin">latest macos build</a>
        <a href="latest/linux">latest linux build</a>
        ${await renderFiletree()}
        <button onclick="fetch('rebuild')">rebuild</button>
      `)
    } else if(_url.pathname.startsWith('/rebuild')) {
      if (building) {
        buildOnceMore = true
        res.writeHead(200)
        res.end('Already building. A second build is queued.')
      } else {
        rebuild(_url.query && _url.query.includes('force'))
        res.writeHead(200)
        res.end('Rebuild started')
      }
    } else if (_url.pathname == '/version') {
      let latest = await getLatest()
      res.writeHead(200)
      res.end(latest['win32'].semver.join('.'))
    } else {
      let filepath = _url.pathname.replace('/build', buildDir)
      let stats
      try {
        stats = await fs.promises.stat(filepath)
      } catch (e) {
        if (e.code == 'ENOENT') {
          res.writeHead(404)
          return res.end('not found')
        }
      }
      let fh = await fs.promises.open(filepath, 'r')
      try {
        let headers = {
          'Content-Length': stats.size,
          'Content-Disposition': 'attachment; filename="' + path.basename(filepath) + '"'
        }
        let contentType = {'.zip':'application/zip','.exe':'application/vnd.microsoft.portable-executable'}[path.extname(filepath)]
        if (contentType)
          headers['Content-Type'] = contentType
        res.writeHead(200, headers)
        await new Promise((resolve, reject) => fs.createReadStream(null, {fd:fh.fd, autoClose:false})
          .on('error', e => reject(e))
          .on('end', e => { fh.close() ; resolve() } )
          .pipe(res)
        )
      } catch (e) {
        fh.close()
        throw e
      }
    }
  }
  catch (e) {
    console.error(e)
    res.writeHead(500)
    res.end('server error')
  }
})

server.listen(config.port, host, () => console.log(`electron ci server running at http://${host}:${config.port}/`))

server.on('error', e => {
  console.log('An error occured while serving: ' + e)
  setTimeout(() => {
      server.close();
      server.listen(config.port, host);
    }, 1000)
})

process.on('SIGINT', function () {
  try {
    server.close()
  } catch(e) { }
  process.exit()
})
