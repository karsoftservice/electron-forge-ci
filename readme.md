
# Electron App Build Server

Make sure your electron app builds with all of

```sh
electron-forge make --platform win32
electron-forge make --platform linux
# macos
electron-forge make --platform darwin
```

If you are on linux and want to build with a working wine setup it may be easier to test the `electron-forge make --platform win32` command inside this docker container. To do that run

```sh
docker run --rm -ti -v $PWD:/home/clone electron-forge-ci /bin/bash
cd /home/clone
./node_modules/.bin/electron-forge make --platform win32
```

Start server with

```sh
./server.js --port=1234 --repository=git@bitbucket.org:abrunnmeier/ci-test.git --sshkey="$(cat /tmp/ci_rsa)"
```

or with docker

```sh
# docker build -t electron-forge-ci .
docker run -d -e CI_PORT=1234 -e CI_REPOSITORY=git@bitbucket.org:abrunnmeier/ci-test.git -e CI_SSHKEY="$(cat /tmp/ci_rsa)" -p 1234:1234 electron-forge-ci
```

To access the built distributables you can volume bind the directory `/home/clone` :

```
docker run -d -v $PWD/clone:/home/clone -e CI_PORT=1234 -e CI_REPOSITORY=git@bitbucket.org:abrunnmeier/ci-test.git -e CI_SSHKEY="$(cat /tmp/ci_rsa)" -p 1234:1234 electron-forge-ci
```

The server depends on git, ssh, nodejs, yarn.

For the packaging, you also need:

- zip: zip
- deb: dpkg, fakeroot
- squirrel: wine (64&32-bit support), mono

# Add SSH key

```sh
ssh-keygen -t rsa -C "electron-ci" -b 4096
```